# clash-auto-switch

[toc]

## 介绍

​	`clash-auto-switch`用于自动切换clash的最优节点。默认每隔1min探测一次，当当前节点不可用或延时大于800ms时，将尝试测量所有节点延时，获取延时最低的节点（优先从指定的可以匹配的节点里选择）。

## 使用

```shell
nsfoxer@nsfoxer-pc ~/.l/b/nf> ./clash-auto-switch -h
自动转换clash代理

Usage: clash-auto-switch [OPTIONS]

Options:
  -c, --controller-api <CONTROLLER_API>
          控制器地址# [default: http://127.0.0.1:9090]
      --secret <SECRET>
          密钥
  -i, --interval <INTERVAL>
          每次测试的时间间隔 单位（秒） [default: 60]
  -u, --url <URL>
          用于网络测试的测试地址 [default: http://www.v2ex.com/generate_204]
      --proxy <PROXY>
          测试网络连通性的代理地址 例如 http://127.0.0.1:7890、 socks5://127.0.0.1:7891 [default: socks5://127.0.0.1:7891]
  -p, --prior-name <PRIOR_NAME>
          优先选择的节点通配符匹配 例如: \*香港\*
  -h, --help
          Print help
```

默认运行：

```shell
nsfoxer@nsfoxer-pc ~/.l/b/nf> ./clash-auto-switch
```

需要查看调试日志：

```shell
nsfoxer@nsfoxer-pc ~/.l/b/nf [SIGINT]> RUST_LOG=debug ./clash-auto-switch
DEBUG [clash_auto_switch::utils::clash_api] config finished! Now node is 台湾 02
DEBUG [clash_auto_switch::utils::connection] [52] Server returned nothing (no headers, no data) (Empty reply from server)
DEBUG [clash_auto_switch] Measure network connection: false delay - 6724ms
DEBUG [clash_auto_switch::utils::clash_api] Nodes List: ["香港 01", "香港 02", "香港 03", "香港 04", "香港 05", "香港 06", "香港 07", "香港 08", "香港 09", "香港 10", "台湾 01", "台湾 02", "日本 01", "日本 02", "日本 03", "日本 04", "日本 05", "日本 06", "日本 07", "
日本 08", "新加坡 01", "新加坡 02", "新加坡 03", "新加坡 04", "美国 01", "美国 02", "美国 03", "美国 04", "美国 05", "美国 06", "美国 07", "美国 08", "加拿大 01", "印度 01", "阿根廷 01"]
ERROR [clash_auto_switch::utils::clash_api] {"加拿大 01": 18446744073709551615, "美国 04": 301, "美国 08": 301, "新加坡 01": 306, "香
港 07": 813, "台湾 02": 18446744073709551615, "新加坡 03": 300, "香港 08": 1223, "香港 06": 1223, "香港 09": 2703, "日本 05": 1222, "
美国 03": 301, "香港 04": 3783, "日本 07": 1428, "日本 04": 1222, "新加坡 04": 300, "阿根廷 01": 447, "香港 10": 2673, "美国 05": 301, "香港 02": 1933, "香港 03": 3369, "印度 01": 338, "日本 02": 1222, "美国 07": 301, "日本 06": 1428, "日本 08": 1529, "美国 02": 301, "美国 06": 300, "美国 01": 301, "台湾 01": 18446744073709551615, "新加坡 02": 300, "日本 03": 1429, "香港 01": 2655, "香港 05": 1121, "日本 01": 1529}
ERROR [clash_auto_switch::utils::clash_api] {"name": "新加坡 03" }
```

