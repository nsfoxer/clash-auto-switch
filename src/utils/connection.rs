use std::time::SystemTime;
use curl::easy::Easy;
use log::{debug};

pub struct NetworkConnection {
    easy: Easy,
}

impl NetworkConnection {
    /// create
    pub fn new(url: &str, proxy: &str) -> Self {
        let mut easy = Easy::new();

        easy.url(url).expect(format!("url {} 设置错误", url).as_str());
        easy.proxy(proxy).expect(format!("proxy {} 设置错误", proxy).as_str());
        easy.get(true).unwrap();

        Self {easy}
    }

    /// Determine if proxy connection is possible
    pub fn is_connect(&self) -> (bool, u128) {
        let start = SystemTime::now();
        if let Err(e) = self.easy.perform() {
            debug!("{}", e);
            (false, start.elapsed().unwrap().as_millis())
        } else {
            (true, start.elapsed().unwrap().as_millis())
        }
    }
}

#[cfg(test)]
mod tests {
    use std::time::Instant;
    use crate::utils::connection::NetworkConnection;

    #[test]
    fn is_network() {
        let network_connection = NetworkConnection::new("http://www.v2ex.com/generate_204", "socks5://127.0.0.1:7891");
        // assert!(network_connection.is_connect());
        let instant = Instant::now();
        let (con, _) = network_connection.is_connect();
        assert!(con);
        let duration = instant.elapsed();

        eprintln!("{}ms", duration.as_millis());
    }
}