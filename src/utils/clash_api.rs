use std::collections::HashMap;
use std::io::Read;
use std::thread::sleep;
use std::time::Duration;
use curl::easy::{Auth, Easy, Easy2, Handler, WriteError};

use anyhow::Result;
use curl::multi::{Easy2Handle, Multi};
use log::{debug, error, info};
use serde_json::{Value};
use percent_encoding::{NON_ALPHANUMERIC, utf8_percent_encode};
use wildmatch::WildMatch;
use crate::MAX_DELAY;

struct Collector(Vec<u8>);

impl Handler for Collector {
    fn write(&mut self, data: &[u8]) -> std::result::Result<usize, WriteError> {
        self.0.extend_from_slice(data);
        Ok(data.len())
    }
}

/// clash api
pub struct ClashApi {
    /// 获取网络详情网络
    proxy_net: Easy,
    /// reset net
    reset_net: Easy,
    /// config的url
    config_url: String,
    /// 测试的所用的url
    test_url: String,
    /// 当前的节点
    now_node: String,
    /// wild word
    wild_word: Option<WildMatch>,
    /// secret
    secret: Option<String>,
}

impl ClashApi {
    pub fn new(config_url: String, secret: Option<String>, test_url: String, prior_name: Option<String>) -> Self {
        // 1. init proxy net
        let mut easy = Easy::new();
        easy.noproxy("*").expect("设置无代理失败");
        easy.url(format!("{}/providers/proxies", config_url).as_str()).expect(format!("url {} 设置失败", config_url).as_str());
        easy.get(true).unwrap();

        // 3. init reset node
        let mut easy3 = Easy::new();
        easy3.noproxy("*").expect("设置无代理失败");
        easy3.put(true).unwrap();
        easy3.url(format!("{}/proxies/Proxies", config_url.as_str()).as_str()).expect("初始化switch url错误");

        // 4. init auth
        if let Some(secret) = &secret {
            let mut auth = Auth::new();
            auth.basic(true);

            easy.password(secret.as_str()).expect("密钥认证设置失败");
            easy.http_auth(&auth).expect("http认证设置失败");
            easy3.password(secret.as_str()).expect("密钥认证设置失败");
            easy3.http_auth(&auth).expect("http认证设置失败");
        }


        // 5. get now node
        let (now_node, _) = Self::get_all_node(&mut easy).expect("获取所有节点失败");
        info!("Now node is {now_node}");
        Self {
            proxy_net: easy,
            reset_net: easy3,
            config_url,
            test_url,
            now_node,
            wild_word: prior_name.map(|x| WildMatch::new(x.as_str())),
            secret,
        }
    }

    pub fn switch_best_node(&mut self) -> Result<bool> {
        // 1. measure
        let (now_node, nodes) = Self::get_all_node(&mut self.proxy_net)?;
        debug!("Nodes List: {:?}", nodes);
        let node_delay_map = self.get_nodes_delay(nodes)?;


        let mut min_delay = u64::MAX;
        let mut best_node = "".to_string();
        info!("{:?}", node_delay_map);
        if let Some(wild) = &self.wild_word {
            let data = node_delay_map.iter()
                .filter(|(node, delay)| wild.matches(node.as_str()) && **delay <= MAX_DELAY as u64)
                .min_by(|a, b| a.1.cmp(b.1));
            if let Some((node, delay)) = data {
                best_node = node.clone();
                min_delay = *delay;
            }
        }

        if min_delay == u64::MAX {
            let option = node_delay_map.into_iter()
                .min_by(|a, b| a.1.cmp(&b.1));
            if let Some(option) = option {
                min_delay = option.1;
                best_node = option.0;
            }
        }


        if min_delay == u64::MAX {
            error!("All nodes are unable to connect!");
            return Ok(false);
        }

        if now_node == best_node {
            info!("Now node is already best node");
            return Ok(false);
        }

        // 3. switch
        if self.switch_node(best_node.as_str())? {
            info!("Now node is switched {best_node}");
            self.now_node = best_node;
            Ok(true)
        } else {
            Ok(false)
        }
    }

    fn build_delay_net(&self, mutl: &mut Multi, token: usize, node: &str) -> Result<Easy2Handle<Collector>> {
        let node_url = utf8_percent_encode(node, NON_ALPHANUMERIC).to_string();
        let url = format!("{}/proxies/{}/delay?timeout=5000&url={}", self.config_url, node_url, self.test_url);

        let mut easy = Easy2::new(Collector(Vec::new()));
        easy.noproxy("*")?;
        easy.get(true)?;
        easy.url(url.as_str())?;

        if let Some(secret) = &self.secret {
            let mut auth = Auth::new();
            auth.basic(true);
            easy.password(secret.as_str()).expect("密钥认证设置失败");
            easy.http_auth(&auth).expect("http认证设置失败");
        }

        let mut handle = mutl.add2(easy)?;
        handle.set_token(token)?;

        Ok(handle)
    }
    fn get_nodes_delay(&mut self, nodes: Vec<String>) -> Result<HashMap<String, u64>> {
        let mut mutl = Multi::new();
        let mut handles = nodes.iter()
            .enumerate()
            .map(|(token, node)| Ok((token, self.build_delay_net(&mut mutl, token, node.as_str())?)))
            .collect::<Result<HashMap<_, _>>>()?;

        let mut node_delay_map = HashMap::with_capacity(nodes.len());

        let mut alive = true;
        while alive {
            alive = mutl.perform()? > 0;

            mutl.messages(|message| {
                let token = message.token().unwrap();
                let handle = handles
                    .get_mut(&token).unwrap();

                match message.result_for2(&handle).unwrap() {
                    Ok(()) => {
                        let status = handle.response_code().unwrap();
                        if status != 200 {
                            node_delay_map.insert(nodes[token].clone(), u64::MAX);
                        } else {
                            let data: Value = serde_json::from_slice(&handle.get_ref().0).unwrap();
                            node_delay_map.insert(nodes[token].clone(), data["delay"].as_u64().unwrap());
                        }
                    }
                    Err(e) => error!("{e}"),
                }
            });
            // 因为perform是非阻塞的，如果不睡眠会导致CPU飙升
            sleep(Duration::from_millis(500));
        }

        Ok(node_delay_map)
    }

    fn switch_node(&mut self, node: &str) -> Result<bool> {
        let body = format!(r#"{{"name": "{}" }}"#, node);
        let mut body = body.as_bytes();

        {
            let mut transfer = self.reset_net.transfer();
            transfer.read_function(|into| {
                Ok(body.read(into).unwrap())
            }).unwrap();
            transfer.perform().unwrap();
        }

        Ok(self.reset_net.response_code().unwrap() == 204)
    }
}

impl ClashApi {
    fn get_all_node(config_net: &mut Easy) -> Result<(String, Vec<String>)> {
        let res = Self::get_http_as_u8(config_net)?;
        let data: Value = serde_json::from_slice(&res)?;

        let mut nodes = Vec::new();
        let mut now_node = "";
        if let Value::Array(value) = &data["providers"]["default"]["proxies"] {
            for value in value {
                // 获取name为 Proxies 的数据
                let value = value.as_object().unwrap();
                if value.get("name").unwrap() != "Proxies" || value.get("type").unwrap() != "Selector" {
                    continue;
                }

                now_node = value.get("now").unwrap().as_str().unwrap();

                value.get("all").unwrap().as_array().unwrap().into_iter().for_each(|x| {
                    nodes.push(String::from(x.as_str().unwrap()));
                });
            }
        }
        Ok((String::from(now_node), nodes))
    }

    fn get_http_as_u8(net: &mut Easy) -> Result<Vec<u8>> {
        let mut buf = Vec::new();
        {
            let mut transfer = net.transfer();
            transfer.write_function(|data| {
                buf.extend_from_slice(data);
                Ok(data.len())
            })?;
            transfer.perform()?;
        }

        Ok(buf)
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;
    use std::io::Read;
    use std::time::SystemTime;
    use curl::easy::Easy;
    use wildmatch::WildMatch;
    use crate::utils::clash_api::ClashApi;

    #[test]
    fn test() {
        let mut api = ClashApi::new(
            "http://127.0.0.1:9000".to_string(),
            None,
            "http://baidu.com".to_string(),
            None,
        );

        api.switch_node("香港 02").expect("TODO: panic message");
    }

    #[test]
    fn test2() {
        let mut data_to_upload = &b"foobar"[..];
        let mut handle = Easy::new();
        handle.url("https://example.com/login").unwrap();
        handle.post(true).unwrap();

        let mut transfer = handle.transfer();
        transfer.read_function(|into| {
            Ok(data_to_upload.read(into).unwrap())
        }).unwrap();
        transfer.perform().unwrap();
    }

    #[test]
    fn test_map() {
        let data = r#"
        {"香港 03":194,"香港 05":104,"香港 08":117,"新加坡 03":304,"新加坡 01":462,"日本 02":134,"美国 06":308,"香港 10":112,"阿根廷 01":455,"日本 07":144,"香港 04":138,"台湾 01":1844674407370955163,"美国 05":245,"美国 07":307,"美国 01":306,"台湾 02":18446744073709551615,"香港 09":170,"美国 04":305,"印度 01":744073709551615,"日本 06":333,"新加坡 02":206,"新加坡 04":203,"香港 07":112,"美国 03":307,"日本 01":218,"日本 04":498,"美国 02":247,"香港 01":317,"日本 03":158,"日本 08":127,"香港 02":135}
        "#;
        let node_delay_map: HashMap<String, u64> = serde_json::from_str(data).unwrap();

        // let mut node_delay_map = HashMap::new();
        let wild = WildMatch::new("*香港*");

        let data = node_delay_map.iter()
            .filter(|(node, delay)| wild.matches(node.as_str()) && **delay < 1000)
            .min_by(|a, b| a.1.cmp(b.1));

        eprintln!("{:?}", data);
    }

    #[test]
    fn test_delay() {
        let mut api = ClashApi::new(
            "http://127.0.0.1:9090".to_string(),
            None,
            "http://www.gstatic.com/generate_204".to_string(),
            None,
        );

        let start = SystemTime::now();
        // let (now_node, nodes) = ClashApi::get_all_node(&mut api.proxy_net).expect("获取所有节点失败");
        let nodes = vec!["美国 05".to_string(),
            "1".to_string(),
                         "2".to_string(),
                         "3".to_string(),
                         "4".to_string(),
        ];
        let map = api.get_nodes_delay(nodes).unwrap();
        eprintln!("{:?}", map);
        eprintln!("{}ms", start.elapsed().unwrap().as_millis());
    }
}
