mod utils;

use std::thread::sleep;
use std::time::Duration;
use clap::Parser;
use log::{error, info, LevelFilter};
use simple_logger::SimpleLogger;
use crate::utils::clash_api::ClashApi;
use crate::utils::connection::NetworkConnection;

pub const MAX_DELAY: usize = 800;

/// 自动转换clash代理
#[derive(Debug, Parser)]
#[clap(about)]
struct Arg {
    /// 控制器地址#
    #[clap(short, long, default_value_t = String::from("http://127.0.0.1:9090"))]
    controller_api: String,

    /// 密钥
    #[clap(long, default_value = None)]
    secret: Option<String>,

    /// 每次测试的时间间隔 单位（秒）
    #[clap(short, long, default_value_t = 60)]
    interval: u64,

    /// 用于网络测试的测试地址
    #[clap(short, long, default_value_t = String::from("http://www.v2ex.com/generate_204"))]
    url: String,

    /// 测试网络连通性的代理地址 例如 http://127.0.0.1:7890、 socks5://127.0.0.1:7891
    #[clap(long, default_value_t = String::from("socks5://127.0.0.1:7891"))]
    proxy: String,

    /// 优先选择的节点通配符匹配 例如: \*香港\*
    #[clap(short, long, default_value = None)]
    prior_name: Option<String>,
}

fn main() {
    // 1. init
    SimpleLogger::new().with_level(LevelFilter::Info).env().init().unwrap();
    let arg = Arg::parse();
    let network_connection = NetworkConnection::new(arg.url.as_str(), arg.proxy.as_str());
    let mut clash_api = ClashApi::new(arg.controller_api, arg.secret, arg.url, arg.prior_name);

    // 2. 开始start loop
    loop {
        if need_replace(&network_connection) {
            if let Err(e) = clash_api.switch_best_node() {
                error!("{e}");
            }
        }
        sleep(Duration::from_secs(arg.interval));
    }
}

fn need_replace(network_connection: &NetworkConnection) -> bool {
    //  1. measure network
    let (connection, delay) = network_connection.is_connect();
    info!("Measure network connection: {connection} delay: {delay}ms");
    if !connection {
        return true;
    }

    delay > MAX_DELAY as u128
}